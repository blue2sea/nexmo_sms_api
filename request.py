import urllib
import urllib2
import json
import random


def nexmo_sms():
    dynamic_sender_ID = str(random.randint(100000000000, 999999999999))
    print dynamic_sender_ID

    params = {
        'api_key': 'API_KEY',
        'api_secret': 'API_SECRET',
        'to': '441632960960',
        'from': dynamic_sender_ID,
        'text': 'Hello'
    }

    url = 'https://rest.nexmo.com/sms/json?' + urllib.urlencode(params)

    request = urllib2.Request(url)
    request.add_header('Accept', 'application/json')
    response = urllib2.urlopen(request)
    print response.msg
    if response.code == 200:
        data = response.read()
        # Decode JSON response from UTF-8
        decoded_response = json.loads(data.decode('utf-8'))
        # Check if your messages are succesful
        messages = decoded_response["messages"]
        for message in messages:
            if message["status"] == "0":
                print "success"
            else:
                print "no success"
    else:
        # Check the errors
        print "unexpected http {code} response from nexmo api".response.code


if __name__ == '__main__':
    nexmo_sms()
